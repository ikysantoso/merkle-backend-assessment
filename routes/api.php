<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\NoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('unauthorized', [AuthController::class, 'unauthorized'])->name('unauthorized');

Route::prefix('auth')->group(function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
});

Route::prefix('guest')->group(function () {
    // need authorized access
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/', [GuestController::class, 'index']);
    });

    Route::post('/', [GuestController::class, 'store']);
});

Route::prefix('note')->group(function () {
    Route::get('/', [NoteController::class, 'index']);
});

// authorized for admin
Route::prefix('admin')->group(function () {
    Route::middleware('auth:sanctum')->group(function () {
        Route::prefix('note')->group(function () {
            Route::get('/', [NoteController::class, 'index']);
        });
    });
});
<?php

namespace App\Http\Controllers;

use App\Repository\NoteRepositoryInterface;
use Exception;

class NoteController extends Controller
{
    protected $repository;

    public function __construct(NoteRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            $res = $this->repository->fetchAll(request()->user());

            return $this->successResponse($res->message, $res->data);
        } catch (Exception $e) {
            return $this->serverErrorResponse($e->getMessage());
        }
    }
}

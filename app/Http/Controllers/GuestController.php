<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGuestRequest;
use App\Repository\GuestRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{
    protected $repository;

    public function __construct(GuestRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        try {
            $res = $this->repository->fetchAll();

            return $this->successResponse($res->message, $res->data);
        } catch (Exception $e) {
            return $this->serverErrorResponse($e->getMessage());
        }
    }

    public function store(StoreGuestRequest $request)
    {
        try {
            DB::beginTransaction();

            $response = $this->repository->create($request);

            if ($response->status) {
                DB::commit();

                return $this->successResponse($response->message, $response->data);
            } else {
                DB::rollback();

                return $this->errorResponse($response->message, $response->data);
            }
        } catch (Exception $e) {
            DB::rollBack();

            return $this->serverErrorResponse($e->getMessage());
        }
    }
}

<?php

namespace App\Repository;

interface AuthRepositoryInterface
{
    public function register($request);
    public function login($request);
}
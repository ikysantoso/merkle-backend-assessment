<?php

namespace App\Repository;

use App\Models\Guest;
use App\Models\Note;
use App\Traits\ServiceResponseHandler;

class GuestRepository implements GuestRepositoryInterface
{
    use ServiceResponseHandler;

    public function fetchAll()
    {
        return $this->successResponse('fetch guests success', Guest::all());
    }

    public function create($request)
    {
        $_req = $request->validated();

        $guest = Guest::create($_req);

        $_note = [
            'guest_id' => $guest->id,
            'note' => $_req['note']
        ];

        Note::create($_note);

        return $this->successResponse('create guests success', $guest);
    }
}
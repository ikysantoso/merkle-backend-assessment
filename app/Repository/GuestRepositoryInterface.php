<?php

namespace App\Repository;

interface GuestRepositoryInterface
{
    public function fetchAll();
    public function create($request);
}
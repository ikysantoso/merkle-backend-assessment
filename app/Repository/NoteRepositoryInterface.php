<?php

namespace App\Repository;

interface NoteRepositoryInterface
{
    public function fetchAll($logged_in);
}
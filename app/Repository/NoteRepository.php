<?php

namespace App\Repository;

use App\Http\Resources\NoteForAdminResource;
use App\Http\Resources\NoteResource;
use App\Models\Note;
use App\Traits\ServiceResponseHandler;

class NoteRepository implements NoteRepositoryInterface
{
    use ServiceResponseHandler;

    public function fetchAll($logged_in)
    {
        // check if admin logged in
        if(isset($logged_in)) {
            // fetch notes with guest relationship for admin
            $notes = Note::with('guest')->get();

            return $this->successResponse('fetch notes success', NoteForAdminResource::collection($notes));
        } else {
            // fetch notes with guest relationship
            $notes = Note::with('guest')->get();

            return $this->successResponse('fetch notes success', NoteResource::collection($notes));
        }
    }
}
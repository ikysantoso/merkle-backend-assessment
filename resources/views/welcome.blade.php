<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env('APP_NAME') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="sm:items-center py-4 sm:pt-0">
            {
                "service": "{{ env('APP_NAME') }}",
                "status": "up",
                "database": "<?php
                    try {
                        \DB::connection()->getPDO();
                        echo 'up';
                        } catch (\Exception $e) {
                        echo 'error';
                    }
                ?>"
            }
        </div>
    </body>
</html>

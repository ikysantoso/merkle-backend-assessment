## How to start the project

1. Run `composer install`
2. Create `.env` file and duplicate the value from `.env.example`
3. Change the `DB_HOST`, `DB_PORT`, `DB_USERNAME`, `DB_PASSWORD` value with your mysql connection
4. Run the `php artisan key:generate`
5. Run the `php artisan migrate`
6. Run the `php artisan serve`

## API Collection

Import the Postman Collection & Postman Env from `api` folder, you can see the responses for each APIs.

## Authentication

1. Register using the `POST register` from `Auth` Collection
2. Login
3. Append the token inside the Authorization using Bearer Token Method

## Technologies
1. Laravel Sanctum for Authentication
2. Laravel Telescope for Logging
3. MySQL Database

